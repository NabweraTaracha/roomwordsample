package com.example.taracha.roomwordsample;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface WordDao {

    /** Declare a method to insert one word:
     *
     * @param word
     */
    @Insert
    void insert(Word word);

    /** Declare a method to delete all the words
     *
     * There is no convenience annotation for deleting multiple entities,
     * so annotate the method with the generic @Query.
     */

    @Query("DELETE FROM word_table")
    void deleteAll();

    /** Create a method to get all the words:
     *
     * Have the method return a List of Words.
     *
     * NOTE: Use a return value of type LiveData in your method description, and Room generates all
     *       necessary code to update the LiveData when the database is updated.
     */

    @Query("SELECT * from word_table ORDER by word ASC")
    LiveData<List<Word>> getAllWords();

}
